# svc-prize-subscription
Service server and database for prize subscription Ruangguru.  
Download postman collection: [https://drive.google.com/file/d/1TgUfCAAFwkb328gaelz3q04RLfTbd0Sy/view?usp=sharing](https://drive.google.com/file/d/1TgUfCAAFwkb328gaelz3q04RLfTbd0Sy/view?usp=sharing)
## How to run locally

### Use npm to install the package manager.
```bash
$ npm install
```

### Ceate database
```bash
$ cd app && npx sequelize-cli db:create
```

### Migrate table
```bash
$ npx sequelize-cli db:migrate
```

### Run seeder
```bash
$ npx sequelize-cli db:seed:all
```

### Run locally
```bash
$ npm run dev
```

## List of user routes:

| Route                     |  HTTP  | Desription                                                  |
| ------------------------- |:------:| ----------------------------------------------------------- |
| /admin/profile            | GET    | Get profile admin data                                      |
| /admin/register           | POST   | Register admin for dashboard                                |
| /admin/login              | POST   | Login admin for dashboard authentication                    |
| /submission               | GET    | Get all submission user                                     |
| /:id                      | GET    | Get detail submission user                                  |
| /submission/toggle-status | PUT    | Toggle status submisssion ('PENDING', 'REJECT', 'DELIVERY') |
| /form?userId=             | GET    | A page for submission prize                                 |
| /form                     | POST   | Create submission for user already subscribe                |

## Usage example
```
api: /admin/register
http: POST

header: {
  token: 'PR1Ze-SuBSCR1pTi0N2021'
}

body: {
  name: '',
  email: '',
  password: '',
}
```

```
api: /admin/login
http: POST

body: {
  email: '',
  password: '',
}
```

```
api: /admin/profile
http: GET
authorization: Bearer Token
```