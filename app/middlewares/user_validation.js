const jwt = require('jsonwebtoken');
const User = require('../models').User;

const checkUserAdmin = async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    const token = authorization.split(' ')[1];
    const decoded = jwt.verify(
      token,
      process.env.SECRET_KEY_TOKEN,
      (err, decoded) => decoded,
    );

    const findUser = await User.findOne({
      where: {id: decoded.id, name: decoded.name, email: decoded.email},
    });

    req.user = findUser;
    return next();
  } catch (error) {
    res.status(400).json({
      success: false,
      message: 'Unauthorized',
    })
  }
};

module.exports = {checkUserAdmin};
