const fs = require('fs');
const CryptoJS = require('crypto-js');

const privateKEY = fs.readFileSync('./app/libs/private.key', 'utf8');

const encrypt = (email, password) => {
  const ciphertext = CryptoJS.AES.encrypt(`${email}${password}`, privateKEY);
  const hash = ciphertext.toString();
  return hash;
};

const decrypt = (password) => {
  const bytes = CryptoJS.AES.decrypt(password, privateKEY);
  const decryptedData = bytes.toString(CryptoJS.enc.Utf8);
  return decryptedData;
};

module.exports = {
  encrypt,
  decrypt,
};
