require('dotenv').config();

const express = require('express');
const enrouten = require('express-enrouten');
const cors = require('cors');
var path = require('path');

const app = express();
const port = process.env.PORT || 3000;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/views//public'));

app.use(cors());

// Body parser
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// Routing
app.use('/', enrouten({ directory: 'routes' }));

// Not found handler
app.use('*', (req, res) => {
  res.status(404).json({
    message: 'Resource not found.',
  });
});

// Error handler
app.use((err, req, res, next) => {
  res.status(500).json(err);
});

app.listen(port, (error, data) => {
  console.log(`App listening on port ${port}`);
});