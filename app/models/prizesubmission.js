'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PrizeSubmission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  PrizeSubmission.init({
    submission_id: DataTypes.INTEGER,
    prize_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'PrizeSubmission',
  });
  return PrizeSubmission;
};