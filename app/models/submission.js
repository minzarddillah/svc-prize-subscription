'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Submission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(
        models.Prize,
        { through: 'PrizeSubmission', foreignKey: `submission_id` }
      );
    }
  };
  Submission.init({
    user_id: DataTypes.STRING,
    delivery_address: DataTypes.STRING,
    msisdn: DataTypes.STRING,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    status: {
      allowNull: false,
      defaultValue: 'PENDING',
      type: DataTypes.ENUM({values: ['PENDING', 'REJECT', 'DELIVERY']}),
    },
  }, {
    sequelize,
    modelName: 'Submission',
  });
  return Submission;
};