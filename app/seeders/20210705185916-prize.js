'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Prizes', [
      {
        name: 'Shoes',
        imageUrl: 'https://static.independent.co.uk/s3fs-public/thumbnails/image/2020/07/28/17/indybest-best-school-shoes.jpg?width=500&auto=webp&quality=75',
        createdAt: new Date(),
        updatedAt: new Date(),
        packageTag: 'englishacademy',
      },
      {
        name: 'Bag',
        imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/91SOSQyJXPL._SY679_.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
        packageTag: 'skillacademy',
      },
      {
        name: 'Pencils',
        imageUrl: 'https://www.eckersleys.com.au/media/catalog/product/cache/image/640x640/e9c3970ab036de70892d86c6d221abfe/1/8/1804311_group.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
        packageTag: 'ruangguru',
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Prizes', null, {});
  }
};
