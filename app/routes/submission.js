const { check, validationResult } = require('express-validator');

const {checkUserAdmin} = require('../middlewares/user_validation');
const Submission = require('../models').Submission;
const Prize = require('../models').Prize;

const getSubmissionListController = async (req, res) => {
  try {
    const result = await Submission.findAll({
      include: [Prize]
    });

    res.status(200).json({
      success: true,
      data: result,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
};

const getSubmissionDetail = async (req, res) => {
  try {
    const result = await Submission.findOne({
      where: {
        id: req.params.id
      },
      include: [Prize],
    });

    res.status(200).json({
      success: true,
      data: result,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
}

const toggleStatusController = async (req, res) => {
  const {status, submissionId} = req.body;

  try {
    validationResult(req).throw();
    const findSubmission = await Submission.findOne({ where: { id: submissionId } });

    if (findSubmission) {
      if (findSubmission.status !== status) {
        const updateSubmission = await Submission.update(
          { status },
          { where: { id: submissionId } }
        );
        res.status(200).json({
          success: true,
          message: `status submission userId ${findSubmission.user_id} has been update to ${status}`
        })
      } else {
        res.status(401).json({
          success: false,
          message: 'status is same',
        })
      }
    } else {
      res.status(404).json({
        success: false,
        message: 'Submission not found',
      });
    }
  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
}

module.exports = (router) => {
  router.get('/', checkUserAdmin, getSubmissionListController);
  router.get('/:id', checkUserAdmin, getSubmissionDetail);
  router.put(
    '/change-status',
    [
      check('status', "status must one of ('PENDING', 'REJECT', 'DELIVERY')").custom((value, { req }) => {
        switch (req.body.status) {
          case 'PENDING':
          case 'REJECT':
          case 'DELIVERY':
            return true;
          default:
            return false;
        }
      }),
      check('submissionId', 'submissionId must not be empty').notEmpty(),
    ],
    checkUserAdmin,
    toggleStatusController
  );
};
