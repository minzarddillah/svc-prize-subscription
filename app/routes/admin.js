const { check, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const { encrypt } = require('../helper/generate_auth');
const { checkUserAdmin } = require('../middlewares/user_validation');
const User = require('../models').User;

const getProfileController = async (req, res) => {
  const user = req.user;

  res.status(200).json({
    success: true,
    data: {
      id: user.id,
      name: user.name,
      email: user.email,
    },
  });
};

const registerAdminUserController = async (req, res) => {
  const dataUser = {
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  };

  if (req.headers.token !== process.env.SECRET_KEY_TOKEN) {
    return res.status(402).json({
      success: false,
      message: 'Unauthorized'
    });
  }

  try {
    validationResult(req).throw();

    const findUser = await User.findOne({
      where: {
        email: dataUser.email,
      }
    });
  
    if (!findUser) {
      dataUser.password = await encrypt(dataUser.email, dataUser.password);
      
      User.create(dataUser);
      
      res.status(200).json({
        success: true,
        message: 'Register Success'
      });
    } else {
      res.status(400).json({
        success: false,
        message: 'email is already exist'
      });
    }
  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
};

const loginAdminUserController = async (req, res) => {
  const body = {
    email: req.body.email,
    password: req.body.password,
  };

  try {
    validationResult(req).throw();
    const findUser = await User.findOne({
      where: {
        email: body.email,
      },
    });

    if (findUser) {
      const token = jwt.sign(
        {
          id: findUser.id,
          name: findUser.name,
          email: findUser.email,
        },
        process.env.SECRET_KEY_TOKEN
      );

      res.status(200).json({
        id: findUser.id,
        name: findUser.name,
        email: findUser.email,
        token
      });
    } else {
      res.status(404).json({
        success: false,
        message: 'Email not registered'
      })
    }
  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
};

module.exports = (router) => {
  router.get('/profile', checkUserAdmin, getProfileController);
  router.post(
    '/register',
    [check('name').notEmpty(), check('email').isEmail(), check('password').notEmpty()],
    registerAdminUserController,
  );
  router.post(
    '/login',
    [check('email').isEmail(), check('password').notEmpty()],
    loginAdminUserController,
  )
};
