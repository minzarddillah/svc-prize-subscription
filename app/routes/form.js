const axios = require('axios');
const { check, validationResult } = require('express-validator');
const { Op } = require('sequelize');

const Submission = require('../models').Submission;
const Prize = require('../models').Prize;
const PrizeSubmission = require('../models').PrizeSubmission;

const getUserDataController = async (req, res) => {
  const { userId } = req.query;

  try {
    const { data } = await axios.get(
      `https://us-central1-silicon-airlock-153323.cloudfunctions.net/rg-package-dummy?userId=${userId}`,
    );

    const benefits = await Prize.findAll({
      where: {
        packageTag: {
          [Op.or]: data.packages.map(({packageTag}) => packageTag)
        }
      }
    });

    const payload = {
      user: data.user,
      benefits: data.packages.map(({packageTag}) => {
        let tmpData = null;

        for (let i = 0; i < benefits.length; i++) {
          if (benefits[i].packageTag === packageTag) {
            tmpData = benefits[i];
          }
        }

        return tmpData;
      }),
    };

    res.render('pages/form', payload);
  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
};

const submitUserDataController = async (req, res) => {
  const {
    userId,
    name,
    email,
    msisdn,
    delivery_address,
  } = req.body;

  try {
    validationResult(req).throw();

    const findSubmissions = await Submission.findOne({
      where: {
        user_id: userId,
      }
    });

    if (!findSubmissions) {
      const { data } = await axios.get(
        `https://us-central1-silicon-airlock-153323.cloudfunctions.net/rg-package-dummy?userId=${userId}`,
      );
      
      const [benefits, newSubmission] = await Promise.all([
        Prize.findAll({
          where: {
            packageTag: {
              [Op.or]: data.packages.map(({packageTag}) => packageTag)
            }
          }
        }),
        Submission.create({
          user_id: userId,
          delivery_address,
          msisdn,
          email,
          name
        })
      ]);

      const dataPrizeSumbission = [];

      for (let i = 0; i < benefits.length; i++) {
        const package = benefits[i];
        dataPrizeSumbission.push({
          submission_id: newSubmission.id,
          prize_id: package.id,
        });
      }

      await PrizeSubmission.bulkCreate(dataPrizeSumbission);

      res.send(`<script> alert('Success'); window.location =  'form?userId=${userId}'; </script>`)
    } else {
      res.json({
        success: false,
        message: 'user already submit',
      });

    }

  } catch (error) {
    res.status(500).json({
      success: false,
      error: JSON.parse(JSON.stringify(error)),
    });
  }
};

module.exports = (router) => {
  router.get('/', getUserDataController);
  router.post(
    '/',
    [
      check('userId').notEmpty(),
      check('name').notEmpty(),
      check('email').notEmpty(),
      check('msisdn').notEmpty(),
      check('delivery_address').notEmpty()
    ],
    submitUserDataController,
  );
};
